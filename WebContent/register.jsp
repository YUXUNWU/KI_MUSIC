<%@ page language="java" pageEncoding="utf-8"%>
<%@ page contentType="text/html;charset=utf-8"%> 
<%
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=utf-8");
%>
<!DOCTYPE html>
<html>
<head>
<title>新用户注册界面</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="./jquery/jquery.js"></script>
<script src="./bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
	<%
		String tipstr = request.getParameter("tip");
		if (tipstr != null)
	%>
	<script type="text/javascript">
     var tipstr="<%=tipstr%>";
		if (tipstr == "0")
			alert("账号已存在，注册失败！");
		if (tipstr == "1")
			alert("注册成功！");
	</script>
	<div class="jumbotron">
		<div class="container">
			<div class="page-header" align="center">
				<h1>新用户注册界面</h1>
			</div>
			<br></br>
			<div>
				<form class="form-horizontal" role="form" name="f" action="register"
					method="post" onsubmit="return check()">
					<div class="form-group">
						<div class="col-sm-12">
							<input type="text" class="form-control input-lg" name="id"
								placeholder="请输入账号">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<input type="text" class="form-control input-lg" name="password"
								placeholder="请输入密码">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-12">
							<button type="submit" class="btn btn-success btn-lg btn-block">提交注册</button>
						</div>
					</div>
					<div class="form-group" style="text-align:center">
						<a href="index.jsp">已经注册？返回登录</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function check() {
			var id = f.id.value;
			var password = f.password.value;
			if (id == "" || id == null || password == "" || password == null) 
			{
				alert("请将信息填写完整！");
				return false;
			}
			else 
			{
				reg=/[`~!@#$%^&*()_+<>?:"{},.\/;'[\]]/im;  
		        if(reg.test(id)||reg.test(password))
		        {  
		            alert("提示：您输入的信息含有非法字符！");  
		            return false; 
		        }
		        else return true;
			}
		}
	</script>
</body>
</html>
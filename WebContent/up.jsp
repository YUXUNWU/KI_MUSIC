<%@ page language="java" pageEncoding="utf-8"%>
<%@ page contentType="text/html;charset=utf-8"%>
<%@ page import="com.wyx.javabean.*"%>
<%
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=utf-8");
%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="./jquery/jquery.js"></script>
<script src="./bootstrap/js/bootstrap.min.js"></script>
<title>上传音乐到酷你</title>
</head>
<body>
<div class="jumbotron">
		<div class="container">
			<div class="page-header" align="center">
				<h1>上传音乐到酷你</h1>
			</div>
			<br></br>
			
			<br></br>
			<%
				userbean loginuser = (userbean) request.getSession().getAttribute("loginuser");//获取登录时保存的Session
				String idstr=new String();
				if (loginuser != null) {
					int id=loginuser.getid();
					idstr=Integer.toString(id);
			%>
			<div style="text-align: center">
				<h5>
					您已登陆！账号：<%=id%></h5>
			</div>
			<%
				} else {
			%>
			<div style="text-align: center">
				<h5>您未登录！登录后才能上传音乐！</h5>
			</div>
			<%
				}
			%>
			<form class="form-horizontal" role="form" name="f" action="upload?file=upload" method="post" enctype="multipart/form-data" onsubmit="return check()"> 
				<div class="form-group">

					<div class="col-sm-12">
						<input type="file" class="form-control input-lg" name="upload"  placeholder="请选择你要上传的文件">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-12">
						<button type="submit" class="btn btn-success btn-lg btn-block">开始上传</button>
					</div>
				</div>
				<div class="form-group" style="text-align:center">
				<a href="musicservlet">返回音乐厅</a>
			</div>
			<h6>小提示：上传操作支持安卓、windows等终端。文件上传需要一定时间，请耐心等待。。。</h6>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		function check() 
		{
			if("<%=idstr%>" != null && "<%=idstr%>" != "")
				{
			var file = f.upload.value;
			if (file != "" && file != null) {
					return true;
				} 
			else {
					alert("请添加待上传文件！");
					return false;
				}
				}
			else 
				{
				alert("您未登录，登录后方可上传音乐！");
				return false;
				}
		}
	</script>
</body>
</html>
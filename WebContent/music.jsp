<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ page import="com.wyx.dao.*"%>
<%@ page import="com.wyx.javabean.*"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.ArrayList"%>
<%
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=utf-8");
%>
<!DOCTYPE html>
<html>
<head>
<title>酷你音乐</title>
<link href="./jPlayer/dist/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />
<script src="./jquery/jquery.js"></script>
<script type="text/javascript" src="./jPlayer/dist/jplayer/jquery.jplayer.min.js"></script>
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="./bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/locale/bootstrap-table-zh-CN.min.js"></script>
<style type="text/css">
.jp-audio {
	width: calc(100% - 42px);
}
td,th,a {
	text-align: center; /*单元格文字居中对齐*/
	color: #444444;
}
html, body {
	width: 100%;
	height: 100%;
	margin: 0;
	padding: 0;
}
.jp-audio {
	margin: 0 auto;
	width: calc(100% - 42px);
}
</style>
</head>
<body>
<h1 style="text-align:center;color:#D200D2">酷你音乐</h1>
<div class="row clearfix" style="align:center">
		<div  style="align:center;margin: auto">
		<%
				userbean loginuser = (userbean) request.getSession().getAttribute("loginuser");//获取登录时保存的Session
			%>
			<form name="f" action="modifypw?id=<%=loginuser.getid()%>"
				method="post" onsubmit="return check()">
			<table data-toggle="table"  data-search-on-enter-key="true" data-striped="true" style="align:center">
				<thead>
					<tr>
						<th>用户账号</th>
						<th>操作一</a></th>
						<th>操作二</a></th>
						<th><input type="text" name="password" placeholder="请输入新密码" style="text-align:center"></th>
					</tr>
				</thead>
				<tbody>
				<tr>
				<td><%=loginuser.getid()%></td>
				<td><a href="index.jsp">登录</a></td>
						<td><a href="logout">注销</a></td>
						<td><button type="submit" class="btn btn-success btn-lg btn-block">改密</button></td>
				</tr>
				</tbody>
			</table>
			</form>
		</div>
	</div>
	<div class="row clearfix">
	<br>
	</div>
<div id="jp_container_1" class="jp-audio" role="application"
		aria-label="media player">
		<div class="row clearfix">
			<div class="col-md-12 column">
				<div class="jp-type-single">
					<div class="jp-progress">
						<div class="jp-seek-bar">
							<div class="jp-play-bar"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a href="#" class="jp-play btn btn-primary btn-sm">播放</a> <a href="#"
			class="jp-pause btn btn-primary btn-sm">暂停</a>
		<div id="jquery_jplayer_1" class="jp-jplayer"></div>
	</div>


	<%
	String idstr=request.getParameter("id");
	String url=new String();
	String name=new String();
	if(idstr!=null&&idstr!="")
	{
		int id = Integer.parseInt(idstr);
		Mydao dao = new Mydao();
		ResultSet rSet = dao.search1(id);
		musicbean musicnow = new musicbean();
		try {
			while (rSet.next()) {
				musicnow.setmusic(rSet.getInt("id"), rSet.getString("name"), rSet.getString("url"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		url = musicnow.geturl();
		System.out.println(url);
		name=musicnow.getname();
		System.out.println(name);
	}
	%>
	
	<div class="row clearfix">
		<div class="col-md-12 column">
			<table data-toggle="table" data-pagination="true" data-search="true" data-search-on-enter-key="true" data-striped="true" data-page-list=[10]>
				<thead>
					<tr>
						<th>编号</th>
						<th>歌名</th>
						<th><a href="musicservlet?id=0">选中播放</a></th>
						<th><a href="download?id=0">选中下载</a></th>
					</tr>
				</thead>
				<tbody>
					<%
							ArrayList<musicbean> musiclist = (ArrayList<musicbean>) request.getAttribute("musiclist");
							for (musicbean music : musiclist) {
						%>
					<tr>
						<td><%=music.getid()%></td>
						<td><%=music.getname()%></td>
						<td><a href="musicservlet?id=<%=music.getid()%>">播放</a></td>
						<td><a href="download?id=<%=music.getid()%>">下载</a></td>
					</tr>
					<%
							}
						%>
				</tbody>
			</table>
			<a style="color: #FFFFFF" href="up.jsp" class="btn btn-success btn-lg btn-block"><h4>我要上传音乐</h4></a>
		</div>
	</div>
	
	<script type="text/javascript">
$(document).ready(function(){
	$("#jquery_jplayer_1").jPlayer({
		ready: function (event) {
			$(this).jPlayer("setMedia",{
				title: "<%=name%>",
				mp3: "<%=url%>",
						autoPlay : true
					}).jPlayer("play");
				},
				swfPath : "./jPlayer/dist/jplayer/jquery.jplayer.swf",
				supplied : "mp3",
				wmode : "window",
				useStateClassSkin : true,
				autoBlur : false,
				smoothPlayBar : true,
				keyEnabled : true,
				remainingDuration : true,
				toggleDuration : true
			});
		});
	</script>
	<script type="text/javascript">
		function check() //提交按钮的检验函数，当用户名、密码为空时弹窗提示错误信息
		{
			var password = f.password.value;
			if (password == "" || password == null) 
			{
				alert("请输入新密码！");
				return false;
			}
			else 
			{
				reg=/[`~!@#$%^&*()_+<>?:"{},.\/;'[\]]/im;  
		        if(reg.test(id)||reg.test(password))
		        {  
		            alert("提示：您输入的新密码含有非法字符！");  
		            return false; 
		        }
		        else return true;
			}
		}
	</script>
</body>
</html>
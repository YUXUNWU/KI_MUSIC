<%@ page language="java" pageEncoding="utf-8"%>
<%@ page contentType="text/html;charset=utf-8"%>
<%
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=utf-8");
%>
<!DOCTYPE html>
<html>
<head>
<title>酷你音乐登录界面</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="./jquery/jquery.js"></script>
<script src="./bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="jumbotron">
		<div class="container">
			<div class="page-header" align="center">
				<h1><a href="index.jsp">酷你音乐</a></h1>
			</div>
			<br></br>
			<form class="form-horizontal" role="form" name="f" action="login"
				method="post" onsubmit="return check()">
				<div class="form-group">

					<div class="col-sm-12">
						<input type="text" class="form-control input-lg" name="myid"
							placeholder="请输入账号">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" class="form-control input-lg" name="password"
							placeholder="请输入密码">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-12">
						<button type="submit" class="btn btn-success btn-lg btn-block">登录</button>
					</div>
				</div>
				<div class="form-group" style="text-align:center">
					<a href="register.jsp">没有账号？请先注册！</a>
				</div>
			</form>
		</div>
	</div>
	<div class="jumbotron">
		<div class="container">
			<h6>TIP：酷你音乐，是一个开放型免费网站。在这里，您可以享受在线听歌、下载音乐资源、上传音乐资源等服务</h6>
		</div>
	</div>
	<script type="text/javascript">
		function check() //提交按钮的检验函数，当用户名、密码为空时弹窗提示错误信息
		{
			var id = f.myid.value;
			var password = f.password.value;
			if (id == "" || id == null || password == "" || password == null) 
			{
				alert("请将信息填写完整！");
				return false;
			}
			else 
			{
				reg=/[`~!@#$%^&*()_+<>?:"{},.\/;'[\]]/im;  
		        if(reg.test(id)||reg.test(password))
		        {  
		            alert("提示：您输入的信息含有非法字符！");  
		            return false; 
		        }
		        else return true;
			}
		}
	</script>
</body>
</html>
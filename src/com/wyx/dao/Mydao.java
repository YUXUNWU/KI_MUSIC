package com.wyx.dao;

import java.sql.*;

import com.wyx.javabean.*;
import com.wyx.util.Myjdbc;

public class Mydao {
	private static Connection con = null;

	public Mydao()
	{
		Myjdbc jdbc = new Myjdbc();
		con = jdbc.jdbc_connect();
	}

	public userbean search_userlogin(int id,String password) {
		String query = "select *from user where id=?";
		ResultSet rSet = null;
		try {
			PreparedStatement pstmt = con.prepareStatement(query); 
			pstmt.setInt(1, id);
			rSet = pstmt.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		userbean user = null;
		try {
			while (rSet.next()) {
				String p = rSet.getString("password");
				if (p.equals(password)) {
					user = new userbean();
					user.setuser(id, p);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	public ResultSet search_userregister(int id) {
		String query = "select *from user where id=?";
		ResultSet rSet = null;
		try {
			PreparedStatement pstmt = con.prepareStatement(query); 
			pstmt.setInt(1, id);
			rSet = pstmt.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return rSet;
	}
	public ResultSet search1(int id) {
		String query = "select *from music where id=?";
		ResultSet rSet = null;
		try {
			PreparedStatement pstmt = con.prepareStatement(query); 
			pstmt.setInt(1, id);
			rSet = pstmt.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rSet;
	}
	public ResultSet search_all() {
		String query = "select *from music";
		ResultSet rSet = null;
		try {
			PreparedStatement pstmt = con.prepareStatement(query); 
			rSet = pstmt.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rSet;
	}
	public int register(int id,String password)
	{
		ResultSet rSet = search_userregister(id);
		try {
			while (rSet.next()) {
				return 0;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String sql = "insert into user(id,password) values(?,?)"; 
		try {
			PreparedStatement pstmt = con.prepareStatement(sql); 
			pstmt.setInt(1, id);
			pstmt.setString(2, password);
			pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 1;
	}
	public int modifypw(int id, String newpassword) {
		String sql = "update user set password=? where id=? "; 
		try {
			PreparedStatement pstmt = con.prepareStatement(sql); 
			pstmt.setInt(2, id);
			pstmt.setString(1, newpassword);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 1;
	}

	public int upload(int i, String filename, String fileurl) {
		String sql = "insert into music(name,url) values(?,?)"; 
		try {
			PreparedStatement pstmt = con.prepareStatement(sql); 
			pstmt.setString(1, filename);
			pstmt.setString(2, fileurl);
			pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 1;
	}
}
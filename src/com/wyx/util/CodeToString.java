package com.wyx.util;

import java.io.UnsupportedEncodingException;

public class CodeToString {
	//处理中文字符串的函数
    public String codeString(String str){
        String s = str;
        try {
            s = new String(s.getBytes("iso8859-1"),"utf-8");
            return s ;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return s;
        }
    }

}

package com.wyx.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyx.dao.*;
import com.wyx.util.*;

/**
 * Servlet implementation class modifypw
 */
@WebServlet("/modifypw")
public class modifypw extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public modifypw() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		Mydao dao = new Mydao();
		int id = Integer.parseInt(request.getParameter("id"));
		String newpassword="";
		try {
			newpassword = MD5.generateCode(request.getParameter("password"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dao.modifypw(id,newpassword);
		response.getWriter().append("<script language='javascript'>");
		response.getWriter().append("alert('改密成功！');");
		response.getWriter().append("window.location.href='musicservlet'</script>");
		//response.sendRedirect("musicservlet");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package com.wyx.servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyx.dao.Mydao;
import com.wyx.util.CodeToString;

/**
 * Servlet implementation class upload
 */
@WebServlet("/upload")
public class upload extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public upload() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("utf-8");
        String tempFileName = new String("tempFileName");
        String string=request.getSession().getServletContext().getRealPath("");
        File tempFile = new File(string+"media/"+tempFileName);
        FileOutputStream outputStream = new FileOutputStream(tempFile);
        InputStream fileSourcel = request.getInputStream();
        byte b[] = new byte[1000];
        int n ;
        while ((n=fileSourcel.read(b))!=-1){
            outputStream.write(b,0,n);
        }
        outputStream.close();
        fileSourcel.close();
        RandomAccessFile randomFile = new RandomAccessFile(tempFile,"r");
        randomFile.readLine();
        String filePath = randomFile.readLine();
        System.out.println(filePath);
        int position = filePath.lastIndexOf('\\');
        int position2 = filePath.lastIndexOf('.');
        int position3 =filePath.lastIndexOf('=');
        System.out.println(position+" "+position2+" "+position3);
        CodeToString codeToString = new CodeToString();
        String filename=new String();
        if(position !=-1 )
        filename = codeToString.codeString(filePath.substring(position+1,position2));
        else filename = codeToString.codeString(filePath.substring(position3+2,position2));
        randomFile.seek(0);
        long  forthEnterPosition = 0;
        int forth = 1;
        while((n=randomFile.readByte())!=-1&&(forth<=4)){
            if(n=='\n'){
                forthEnterPosition = randomFile.getFilePointer();
                forth++;
            }
        }
        File saveFile = new File(string+"/media",filename+".mp3");
        RandomAccessFile randomAccessFile = new RandomAccessFile(saveFile,"rw");
        randomFile.seek(randomFile.length());
        long endPosition = randomFile.getFilePointer();
        int j = 1;
        while((endPosition>=0)&&(j<=4)){
            endPosition--;
            randomFile.seek(endPosition);
            if(randomFile.readByte()=='\n'){
                j++;
            }
        }
        
        randomFile.seek(forthEnterPosition);
        long startPoint = randomFile.getFilePointer();
        while(startPoint<endPosition){
            randomAccessFile.write(randomFile.readByte());
            startPoint = randomFile.getFilePointer();
        }
        randomAccessFile.close();
        randomFile.close();
        tempFile.delete();
        
		String fileurl = "media/"+filename+".mp3";
		 System.out.println(fileurl);
		Mydao dao = new Mydao();
		int tip = dao.upload(0,filename, fileurl);
        response.getWriter().append("<script language='javascript'>");
		response.getWriter().append("alert('恭喜您！上传成功！');");
		response.getWriter().append("window.location.href='up.jsp'</script>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

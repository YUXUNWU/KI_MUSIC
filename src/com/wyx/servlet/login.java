package com.wyx.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyx.dao.*;
import com.wyx.javabean.*;
import com.wyx.util.*;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		Mydao dao = new Mydao();
		String password = request.getParameter("password");
		String MD5password="";
		try {
			MD5password=MD5.generateCode(password);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String idstr= request.getParameter("myid");
		int id = Integer.parseInt(idstr);
		userbean user = dao.search_userlogin(id, MD5password);//MD5密码保护
		if (user != null)
		{
			request.getSession().setAttribute("loginuser", user);
			request.getRequestDispatcher("musicservlet").forward(request, response);
		
		}else
		{
			response.getWriter().append("<script language='javascript'>");
			response.getWriter().append("alert('编号或密码错误，请重新输入！');");
			response.getWriter().append("window.location.href='index.jsp'</script>");
		}
	}

}

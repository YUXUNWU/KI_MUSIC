package com.wyx.servlet;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyx.dao.Mydao;
import com.wyx.javabean.musicbean;

/**
 * Servlet implementation class musicservlet
 */
@WebServlet("/musicservlet")
public class musicservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public musicservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html;charset=utf-8");// 设置响应的字符集
		request.setCharacterEncoding("utf-8");// 设置请求的字符集
		Mydao dao = new Mydao();
		ResultSet rSet = dao.search_all();
		ArrayList<musicbean> musiclist = new ArrayList<musicbean>();
		try {
			while (rSet.next()) {
				musicbean myclient = new musicbean();
				myclient.setmusic(rSet.getInt("id"), rSet.getString("name"), rSet.getString("url"));
				musiclist.add(myclient);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		request.setAttribute("musiclist", musiclist);
		String idstr=request.getParameter("id");
		if(idstr!=null&&idstr!="")
		{
			int id = Integer.parseInt(idstr);
			request.getRequestDispatcher("music.jsp?id=<%=id%>").forward(request, response);
		}
		
		else
		request.getRequestDispatcher("music.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
